//Atributos

var variableSlide = {
    paginacion: document.querySelectorAll("#paginacion ul li"),
    cajaSlide: document.querySelector("#slide .caja"),
    img1: document.querySelector("#slide .caja .caja1"),
    img2: document.querySelector("#slide .caja .caja2"),
    img3: document.querySelector("#slide .caja .caja3"),
    img4: document.querySelector("#slide .caja .caja4"),
    img5: document.querySelector("#slide .caja .caja5"),
    img6: document.querySelector("#slide .caja .caja6"),
    next1: document.querySelector("#circulo-go"),
    next2: document.querySelector("#direccion-go"),
    back1: document.querySelector("#circulo-back"),
    back2: document.querySelector("#direccion-back"),
    elementoTouch: document.querySelector(".caja"),
    animacionSilde: "slide",
    arreglo: [],
    item: 0,
    dimension: 0,
    contador: 0,
    original: 0,
    estado: 0
};


//Metodos u Operaciones 

var operaSlide = {

    inicioSlide: function() {

        //Arreglo con las cajas de comentarios individual para saber tamaño de coleccion
        variableSlide.arreglo = [variableSlide.img1, variableSlide.img2, variableSlide.img3, variableSlide.img4, variableSlide.img5, variableSlide.img6];


        // Ciclo for para detectar los eventos click
        for (var i = 0; i < variableSlide.paginacion.length; i++) {

            variableSlide.paginacion[i].addEventListener("click", operaSlide.paginacionSlide);

        }

        // Eventos click de siguiente y retroceder
        variableSlide.next1.addEventListener("click", operaSlide.moveNext);
        variableSlide.next2.addEventListener("click", operaSlide.moveNext);

        variableSlide.back1.addEventListener("click", operaSlide.moveBack);
        variableSlide.back2.addEventListener("click", operaSlide.moveBack);

        variableSlide.elementoTouch.addEventListener("touchmove", operaSlide.tactil);

        // document.addEventListener("keydown", operaSlide.oprimir);


    },


    paginacionSlide: function(item) {

        // obtener el numero de orden de los li
        variableSlide.item = item.target.parentNode.getAttribute("item") - 1;
        operaSlide.toogle(); // verificar funcion de tamaño de ventana

    },


    moveNext: function() {

        //Si el valor del item es igual al último-1 del arreglo quiere decir que es el
        //último diapositiva, volverlo 0 es decir al inicio cuando se de en siguiente
        if (variableSlide.item == variableSlide.arreglo.length - 1) {

            variableSlide.item = 0;

        } else {

            variableSlide.item++;

        }

        operaSlide.movimientoSlide(variableSlide.item);
    },

    moveBack: function() {

        //Si la variable item es 0 es decir esta al inicio, al retroceder convertirlo al valor
        //último-1 del arreglo es decir mandarlo al final
        if (variableSlide.item == 0) {

            variableSlide.item = variableSlide.arreglo.length - 1;

        } else {

            variableSlide.item--;

        }

        operaSlide.movimientoSlide(variableSlide.item);

    },



    movimientoSlide: function(item) {

        operaSlide.numeroClicks(); //inicializa el contador de click
        operaSlide.contarBotones(); //inicializa el contador de click


        //Reducir la opaciodad de los indices de paginacion a .5
        for (var i = 0; i < variableSlide.paginacion.length; i++) {

            variableSlide.paginacion[i].style.opacity = 0.5;
        }

        variableSlide.paginacion[item].style.opacity = 1; //solo mantener en 1 el actual


        if (variableSlide.animacionSilde == "slide") {

            //si el numero de clicks es igual a 1 es decir al inicio cuando se recarga la pagina
            if (variableSlide.contador == 1) {

                variableSlide.cajaSlide.style.left = item * 0 + "%";

                variableSlide.cajaSlide.style.transition = "1s left ease";

                setTimeout(function() {

                    variableSlide.cajaSlide.style.left = item * -85 + "%";

                }, 500);

            } else {
                variableSlide.cajaSlide.style.left = item * -85 + "%";

                variableSlide.cajaSlide.style.transition = "1s left ease";
            }

        }


        if (variableSlide.animacionSilde == "fade") {

            variableSlide.arreglo[item].style.opacity = 0;

            variableSlide.arreglo[item].style.transition = ".7s opacity ease";

            setTimeout(function() {

                variableSlide.arreglo[item].style.opacity = 1;

            }, 500);

        }

    },


    //Calcular las resoluciones de la ventana en tiempo real
    calcularVentana: function() {
        variableSlide.dimension = window.innerWidth;
        variableSlide.original = window.screen.width;
        return variableSlide.dimension;

    },


    // Verificar que solo funcione en pantallas con viewport menor a 1281
    toogle: function() {

        // console.log(variableSlide.dimension);
        // console.log(variableSlide.original);



        if (variableSlide.dimension < 1281 || variableSlide.original < 1281) {
            operaSlide.movimientoSlide(variableSlide.item);

        }

        if (variableSlide.dimension > 1280 || variableSlide.original > 1280) {
            console.log("En construccion");
        }

    },

    numeroClicks: function() {
        if (variableSlide.paginacion) {
            variableSlide.contador++;
        }
        return variableSlide.contador;
    },


    //Contar las veces que se pulsa sobre los botones next o back 
    contarBotones: function() {
        if (variableSlide.next1 || variableSlide.next2 || variableSlide.back1 || variableSlide.back2) {
            variableSlide.estado++;
        }
        return variableSlide.estado;
    },


    //Usar las teclas direccionales para mover
    oprimir: function(event) {

        if (variableSlide.contador > 0 || variableSlide.estado > 0) {
            switch (event.key) {
                case "ArrowRight":
                    operaSlide.moveNext();
                    break;

                case "ArrowLeft":
                    operaSlide.moveBack();
                    break;
            }
        }
    },

    //usar el tactil de la pantalla
    tactil: function(event) {
        console.log("tactil".event);

    }


};

//Ejecutar funcion
operaSlide.inicioSlide();