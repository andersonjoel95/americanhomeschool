//Atributos

var desktop = {
    paginacion: document.querySelectorAll("#paginacion-row ul li"),
    cajaSlide: document.querySelector("#contenedor-slide-row section"),
    img1: document.querySelector("#contenedor-slide-row section.caja1-row"),
    img2: document.querySelector("#contenedor-slide-row section.caja2-row"),
    img3: document.querySelector("#contenedor-slide-row section.caja3-row"),
    img4: document.querySelector("#contenedor-slide-row section.caja4-row"),
    img5: document.querySelector("#contenedor-slide-row section.caja5-row"),
    img6: document.querySelector("#contenedor-slide-row section.caja6-row"),
    animacionSilde: "slide",
    arreglo: [],
    item: 0,
    dimension: 0,
    contador: 0,
    original: 0,
    estado: 0,
    angulo: 0,
    velocidadSlide: 5000,
    formatearLoop: false
};

var operaDesktop = {

    inicioSlide: function() {

        desktop.arreglo = [variableSlide.img1, variableSlide.img2, variableSlide.img3, variableSlide.img4, variableSlide.img5, variableSlide.img6];


        // Ciclo for para detectar los eventos click
        for (var i = 0; i < desktop.paginacion.length; i++) {

            desktop.paginacion[i].addEventListener("click", operaDesktop.paginacionSlide);

        }

        // document.addEventListener("keydown", operaDesktop.oprimir);
        operaDesktop.intervalo();

    },


    paginacionSlide: function(item) {

        // obtener el numero de orden de los li
        desktop.item = item.target.parentNode.getAttribute("item") - 1;
        operaDesktop.movimientoSlide(desktop.item);

    },


    moveNext: function() {

        //Si el valor del item es igual al último-1 del arreglo quiere decir que es el
        //último diapositiva, volverlo 0 es decir al inicio cuando se de en siguiente
        if (desktop.item == desktop.arreglo.length - 1) {

            desktop.item = 0;

        } else {

            desktop.item++;

        }

        operaDesktop.movimientoSlide(desktop.item);
    },

    moveBack: function() {

        //Si la variable item es 0 es decir esta al inicio, al retroceder convertirlo al valor
        //último-1 del arreglo es decir mandarlo al final
        if (desktop.item == 0) {

            desktop.item = desktop.arreglo.length - 1;

        } else {

            desktop.item--;

        }

        operaDesktop.movimientoSlide(desktop.item);

    },



    movimientoSlide: function(item) {


        operaSlide.numeroClicks(); //inicializa el contador de click
        operaDesktop.contarBotones(); //inicializa el contador de click

        //Reducir la opaciodad de los indices de paginacion a .5
        for (var i = 0; i < desktop.paginacion.length; i++) {

            desktop.paginacion[i].style.opacity = 0.5;
        }

        desktop.paginacion[item].style.opacity = 1; //solo mantener en 1 el actual


        //si el numero de clicks es igual a 1 es decir al inicio cuando se recarga la pagina

        // console.log("antes de switch", item);

        switch (item) {
            case 0:
                desktop.angulo = 0;

                desktop.img1.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo + "deg); transform: rotateY(" + desktop.angulo + "deg);");
                desktop.img2.setAttribute("style", "-webkit-transform: rotateY(-60deg); transform: rotateY(-60deg);");
                desktop.img3.setAttribute("style", "-webkit-transform: rotateY(-120deg); transform: rotateY(-120deg);");
                desktop.img4.setAttribute("style", "-webkit-transform: rotateY(-180deg); transform: rotateY(-180deg);");
                desktop.img5.setAttribute("style", "-webkit-transform: rotateY(-240deg); transform: rotateY(-240deg);");
                desktop.img6.setAttribute("style", "-webkit-transform: rotateY(-300deg); transform: rotateY(-300deg);");

                break;

            case 1:
                desktop.angulo = desktop.angulo + 60;

                desktop.img1.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo + "deg); transform: rotateY(" + desktop.angulo + "deg); z-index:" + -(desktop.item * 100));
                desktop.img2.setAttribute("style", "-webkit-transform: rotateY(" + -(desktop.angulo * 0) + "deg); transform: rotateY(" + -(desktop.angulo * 0) + "deg); z-index:" + desktop.item * 300);
                desktop.img3.setAttribute("style", "-webkit-transform: rotateY(" + -(desktop.angulo) + "deg); transform: rotateY(" + -(desktop.angulo) + "deg); z-index:" + desktop.item * 200 + "; background: rgba(255,255,255,1);");
                desktop.img4.setAttribute("style", "-webkit-transform: rotateY(" + -(desktop.angulo * 2) + "deg); transform: rotateY(" + -(desktop.angulo * 2) + "deg); z-index:" + desktop.item * 100);
                desktop.img5.setAttribute("style", "-webkit-transform: rotateY(" + -(desktop.angulo * 3) + "deg); transform: rotateY(" + -(desktop.angulo * 3) + "deg); z-index:" + -(desktop.item * 300));
                desktop.img6.setAttribute("style", "-webkit-transform: rotateY(" + -(desktop.angulo * 4) + "deg); transform: rotateY(" + -(desktop.angulo * 4) + "deg); z-index:" + -(desktop.item * 200) + "; opacity:.6;");

                desktop.angulo = 0;
                break;

            case 2:
                desktop.angulo = desktop.angulo + 120;

                desktop.img1.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo + "deg); transform: rotateY(" + desktop.angulo + "deg); z-index:" + -(desktop.item * 200) + "; opacity:.6;");
                desktop.img2.setAttribute("style", "-webkit-transform: rotateY(" + (desktop.angulo / 2) + "deg); transform: rotateY(" + (desktop.angulo / 2) + "deg); z-index:" + -(desktop.item * 100));
                desktop.img3.setAttribute("style", "-webkit-transform: rotateY(" + (desktop.angulo * 0) + "deg); transform: rotateY(" + (desktop.angulo * 0) + "deg); z-index:" + desktop.item * 300 + "; background:rgba(255,255,255,1);");
                desktop.img4.setAttribute("style", "-webkit-transform: rotateY(" + -(desktop.angulo / 2) + "deg); transform: rotateY(" + -(desktop.angulo / 2) + "deg); z-index:" + desktop.item * 200 + ";background:rgba(255,255,255,1);");
                desktop.img5.setAttribute("style", "-webkit-transform: rotateY(" + -(desktop.angulo) + "deg); transform: rotateY(" + -(desktop.angulo) + "deg);z-index:" + desktop.item * 100 + ";");
                desktop.img6.setAttribute("style", "-webkit-transform: rotateY(" + -(desktop.angulo + 60) + "deg); transform: rotateY(" + -(desktop.angulo + 60) + "deg); z-index:" + -(desktop.item * 300) + "; opacity:.6;");

                desktop.angulo = 0;
                break;

            case 3:

                desktop.angulo = desktop.angulo + 180;

                desktop.img1.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo + "deg); transform: rotateY(" + desktop.angulo + "deg); z-index:" + -(desktop.item * 300) + "; opacity:.6;");
                desktop.img2.setAttribute("style", "-webkit-transform: rotateY(" + (desktop.angulo - 60) + "deg); transform: rotateY(" + (desktop.angulo - 60) + "deg); z-index:" + -(desktop.item * 200) + "; opacity:.6;");
                desktop.img3.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo / 3 + "deg); transform: rotateY(" + desktop.angulo / 3 + "deg); z-index:" + -(desktop.item * 100) + "; background:rgba(255,255,255,1);");
                desktop.img4.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo * 0 + "deg); transform: rotateY(" + desktop.angulo * 0 + "deg);z-index:" + desktop.item * 300 + "; background:rgba(255,255,255,1);");
                desktop.img5.setAttribute("style", "-webkit-transform: rotateY(" + -(desktop.angulo / 3) + "deg); transform: rotateY(" + -(desktop.angulo / 3) + "deg);z-index:" + desktop.item * 200 + ";background:rgba(255,255,255,1);");
                desktop.img6.setAttribute("style", "-webkit-transform: rotateY(" + -(desktop.angulo - 60) + "deg); transform: rotateY(" + -(desktop.angulo - 60) + "deg);z-index:" + desktop.item * 100 + "; opacity:.6;");

                desktop.angulo = 0;
                break;

            case 4:

                desktop.angulo = desktop.angulo + 240;

                desktop.img1.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo + "deg); transform: rotateY(" + desktop.angulo + "deg);z-index:" + desktop.item * 100 + "; opacity:.6;");
                desktop.img2.setAttribute("style", "-webkit-transform: rotateY(" + (desktop.angulo - 60) + "deg); transform: rotateY(" + (desktop.angulo - 60) + "deg);z-index:" + -(desktop.item * 300) + "; opacity:.6;");
                desktop.img3.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo / 2 + "deg); transform: rotateY(" + desktop.angulo / 2 + "deg);z-index:" + -(desktop.item * 200) + "; opacity:.6;");
                desktop.img4.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo / 4 + "deg); transform: rotateY(" + desktop.angulo / 4 + "deg);z-index:" + -(desktop.item * 100) + "; background:rgba(255,255,255,1);");
                desktop.img5.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo * 0 + "deg); transform: rotateY(" + desktop.angulo * 0 + "deg);z-index:" + desktop.item * 300 + "; background:rgba(255,255,255,1);");
                desktop.img6.setAttribute("style", "-webkit-transform: rotateY(" + -(desktop.angulo / 4) + "deg); transform: rotateY(" + -(desktop.angulo / 4) + "deg);z-index:" + desktop.item * 200 + ";background:rgba(255,255,255,1);");

                desktop.angulo = 0;
                break;

            case 5:

                desktop.angulo = desktop.angulo + 300;

                desktop.img1.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo + "deg); transform: rotateY(" + desktop.angulo + "deg); z-index:" + desktop.item * 200 + ";");
                desktop.img2.setAttribute("style", "-webkit-transform: rotateY(" + (desktop.angulo - 60) + "deg); transform: rotateY(" + (desktop.angulo - 60) + "deg);z-index:" + desktop.item * 100 + "; opacity:.6;");
                desktop.img3.setAttribute("style", "-webkit-transform: rotateY(" + (desktop.angulo - 120) + "deg); transform: rotateY(" + (desktop.angulo - 120) + "deg);z-index:" + -(desktop.item * 300) + "; opacity:.6;");
                desktop.img4.setAttribute("style", "-webkit-transform: rotateY(" + (desktop.angulo - 180) + "deg); transform: rotateY(" + (desktop.angulo - 180) + "deg);z-index:" + -(desktop.item * 200) + ";");
                desktop.img5.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo / 5 + "deg); transform: rotateY(" + desktop.angulo / 5 + "deg);z-index:" + -(desktop.item * 100) + "; background:rgba(255,255,255,1);");
                desktop.img6.setAttribute("style", "-webkit-transform: rotateY(" + desktop.angulo * 0 + "deg); transform: rotateY(" + desktop.angulo * 0 + "deg);z-index:" + desktop.item * 300 + "; background:rgba(255,255,255,1);");

                desktop.angulo = 0;
                break;

        }

    },


    numeroClicks: function() {
        if (variableSlide.paginacion) {
            variableSlide.contador++;
        }
        return variableSlide.contador;
    },


    contarBotones: function() {
        if (desktop.paginacion) {
            desktop.estado++;
        }
        return desktop.estado;
    },

    oprimir: function(event) {

        console.log(event.key);

        if (desktop.estado > 0) {
            switch (event.key) {
                case "ArrowRight":
                    operaDesktop.moveNext();
                    break;

                case "ArrowLeft":
                    operaDesktop.moveBack();
                    break;
            }
        }
    },

    intervalo: function() {

        setInterval(function() {

            if (desktop.formatearLoop) {

                desktop.formatearLoop = false;

            } else {

                operaDesktop.moveNext();

            }

        }, desktop.velocidadSlide);

    }


};

//Ejecutar funcion
operaDesktop.inicioSlide();