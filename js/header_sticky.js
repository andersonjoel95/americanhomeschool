//objeto con valores
var p = {
    logo: document.querySelector("header nav div"),
    navegador: document.querySelector("header .barra"),
    distancia: null
};


//objeto con metodos
var m = {
    inicio: function() {
        document.addEventListener("scroll", m.calcularPixeles);
    },

    // calcularPixeles: function(event) {
    //     console.log(event);
    //     distancia = window.scrollY;
    //     console.log(distancia);
    // }

    calcularPixeles: function() {
        if (window.scrollY > 0) {
            p.navegador.style.opacity = "0.9";
        } else {
            p.navegador.style.opacity = "1";

        }
    }

};

m.inicio();