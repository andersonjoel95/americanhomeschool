var list = {

    barra1: document.querySelector("#local-first"),
    barra2: document.querySelector("#local-second"),
    barra3: document.querySelector("#local-third"),
    barra4: document.querySelector("#local-fourth"),
    barra5: document.querySelector("#local-fifth"),
    barra6: document.querySelector("#local-sixth"),
    contador: 0,
    liSelect: document.getElementsByClassName("free"),
    spanSelect: document.getElementsByClassName("frase"),
    iconSelect: document.getElementsByClassName("extra"),

    cajaSelect: document.getElementsByClassName("fas fa-plus-circle"),
    valor: null,
    aux: null
};

var listOpera = {
    inicio: function() {
        //list.liSelect = addEventListener("click", listOpera.mostrarRespuesta); //evento click general al DOM
        //list.spanSelect = addEventListener("click", listOpera.mostrarRespuesta);
        //list.iconSelect = addEventListener("click", listOpera.mostrarRespuesta);
        //list.cajaSelect = addEventListener("click", listOpera.mostrarRespuesta);

        //Evento click sobre la lista de colecciones 
        for(var i=0; i<list.liSelect.length;i++){
            list.liSelect[i].addEventListener("click",listOpera.mostrarRespuesta);
        }

    },


    mostrarRespuesta: function() {
        list.valor = event.target.getAttribute("id");

        switch (list.valor) {
            case "first":
                listOpera.toggle(list.barra1);
                break;
            case "second":
                listOpera.toggle(list.barra2);
                break;
            case "third":
                listOpera.toggle(list.barra3);
                break;
            case "fourth":
                listOpera.toggle(list.barra4);
                break;
            case "fifth":
                listOpera.toggle(list.barra5);
                break;
            case "sixth":
                listOpera.toggle(list.barra6);
                break;

        }

    },

    mostrar: function(elem) {

        // Obtener la altura real del elemento 

        var getHeight = function() {
            elem.style.display = "block"; // Hacerlo visible
            var height = elem.scrollHeight + "px"; // Obtener el valor de la altura mediante scroll y concaternar con px
            elem.style.display = ""; //  Ocultarlo de nuevo
            return height;
        };

        var height = getHeight(); // Obtener la altura real
        elem.classList.add('visible'); // Añadirle la clase visible que contiene las caracteristicas del elemento
        elem.style.height = height; // actualizar su max-hight

        // Once the transition is complete, remove the inline max-height so the content can scale responsively
        // Una vez que se complete la transición, elimine la altura máxima en línea para que el contenido 
        //pueda escalar de manera receptiva

        window.setTimeout(function() {
            elem.style.height = "";
        }, 100);

    },


    ocultar: function(elem) {

        //Obtener el elemento real a cambiar
        elem.style.height = elem.scrollHeight + 'px';

        // Settear la altura a 0
        window.setTimeout(function() {
            elem.style.height = '0';
        }, 1);

        //Cuando la transicion termine, remover la clase que hace visible el elemento, es decir, ocultarlo
        window.setTimeout(function() {
            elem.classList.remove('visible');
        }, 1);

    },



    toggle: function(elem) {

        //Si el elemento es visible, ocultalo
        if (elem.classList.contains("visible")) {

            listOpera.ocultar(elem);

            return;

            //Si no es visible, mostrarlo
        } else {

            listOpera.mostrar(elem);
        }

    }

};

listOpera.inicio(); //ejecuta funcion

// if (list.barra.classList.contains("end")) {
//     list.barra.classList.remove("end");
// } else {
//     console.log("No se encuentra .end")
// }