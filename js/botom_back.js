var botom = {
    distancia: null,
    btnRetorna: document.querySelector("#retornar i"),
    timeOut: null
};

var botomOperation = {
    inicio: function() {
        document.addEventListener("scroll", botomOperation.cuenta);
        botom.btnRetorna.addEventListener("click", botomOperation.regresar);
    },
    cuenta: function() {
        // distancia = window.scrollY;
        // console.log("botom-back", distancia);
        if (window.scrollY < 210) {
            botom.btnRetorna.style.display = "none";
        } else {
            botom.btnRetorna.style = "none";

        }

    },
    regresar: function() {
        if (document.body.scrollTop != 0 || document.documentElement.scrollTop != 0) {
            window.scrollBy(0, -50);
            botom.timeOut = setTimeout(function(){
                botomOperation.regresar();}, 10);
        } else {
            clearTimeout(botom.timeOut);
        }
    }



};

botomOperation.inicio();